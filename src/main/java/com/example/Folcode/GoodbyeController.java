package com.example.Folcode;

import org.springframework.web.bind.annotation.*;

@RestController
public class GoodbyeController {
    @GetMapping("/goodbye")
    public String goodbye(){
        return "Get Goodbye world";
    }

    @PostMapping ("/goodbye")
    @ResponseBody
    public String postGoodbye(){
        return "Post Goodbye world";
    }

    @RequestMapping("/adios")
    public String adios(){
        return "Adios";
    }
}
