package com.example.Folcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FolcodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(FolcodeApplication.class, args);
	}

}
